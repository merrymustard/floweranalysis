import pandas as pd
import matplotlib.pyplot as plt

# Read csv file
data = pd.read_csv("iris.csv")

# Categories of flowers
setosa_group = data[data['class'] == 'Iris-setosa']
versicolor_group = data[data['class'] == 'Iris-versicolor']
virginica_group = data[data['class'] == 'Iris-virginica']

fig = plt.figure()
ax1 = fig.add_subplot(111)

# Plot #1
ax1.scatter(x=setosa_group['sepal_length'], y=setosa_group['sepal_width'], c='b', label='setosa')
ax1.scatter(x=versicolor_group['sepal_length'], y=versicolor_group['sepal_width'], c='r', label='versicolor')
ax1.scatter(x=virginica_group['sepal_length'], y=virginica_group['sepal_width'], c='g', label='virginica')

plt.legend()
plt.xlabel('Sepal length')
plt.ylabel('Sepal width')
plt.show()

fig = plt.figure()
ax1 = fig.add_subplot(111)

# These two features are correlated because they show a linear graph
# Plot #2
ax1.scatter(x=setosa_group['petal_length'], y=setosa_group['petal_width'], c='b', label='setosa')
ax1.scatter(x=versicolor_group['petal_length'], y=versicolor_group['petal_width'], c='r', label='versicolor')
ax1.scatter(x=virginica_group['petal_length'], y=virginica_group['petal_width'], c='g', label='virginica')

plt.legend()
plt.xlabel('Petal length')
plt.ylabel('Petal width')
plt.show()

fig = plt.figure()
ax1 = fig.add_subplot(111)

# Plot #3
ax1.scatter(x=setosa_group['sepal_length'], y=setosa_group['petal_width'], c='b', label='setosa')
ax1.scatter(x=versicolor_group['sepal_length'], y=versicolor_group['petal_width'], c='r', label='versicolor')
ax1.scatter(x=virginica_group['sepal_length'], y=virginica_group['petal_width'], c='g', label='virginica')

plt.legend()
plt.xlabel('Sepal length')
plt.ylabel('Petal width')
plt.show()

fig = plt.figure()
ax1 = fig.add_subplot(111)

# Plot #4
ax1.scatter(x=setosa_group['petal_width'], y=setosa_group['sepal_length'], c='b', label='setosa')
ax1.scatter(x=versicolor_group['petal_width'], y=versicolor_group['sepal_length'], c='r', label='versicolor')
ax1.scatter(x=virginica_group['petal_width'], y=virginica_group['sepal_length'], c='g', label='virginica')

plt.legend()
plt.xlabel('Petal width')
plt.ylabel('Sepal length')
plt.show()