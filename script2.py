import pandas as pd

# Read csv file
data = pd.read_csv("2015_rendafam-a.csv", encoding="utf-8")

# Get an outlook of the data
print(data.info())

isna = data['District'].isna()
missing_dist= data[isna]
print("Columns w/ missing district")
print(missing_dist)
print("\n")

isna = data['Neighborhood'].isna()
missing_neigh= data[isna]
print("Columns w/ missing neighborhood")
print(missing_neigh)
print("\n")

isna = data['Population'].isna()
missing_pop= data[isna]
print("Columns w/ missing population")
print(missing_pop)
print("\n")

isna = data['income_index'].isna()
missing_index= data[isna]
print("Columns w/ missing income_index")
print(missing_index)

